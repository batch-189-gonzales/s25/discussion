/*
	JSON Objects
		-stands for JavaScript Object Notation
		-also used in other programming languages

	Syntax:
		{
			"property1": "value1",
			"property2": "value2",
			...
			"propertyN": "valueN"
		}
*/

//JSON Object
/*{
	"city": "Quezon City",
	"province": "Metro Manila",
	"country": "Philippines"
};*/

//JSON Array
/*"cities" = [
	{"city": "Quezon City", "province": "Metro Manila", "country": "Philippines"},
	{"city": "City of Manila", "province": "Metro Manila", "country": "Philippines"},
	{"city": "Makati City", "province": "Metro Manila", "country": "Philippines"}
];*/

//JSON METHODS
let batchesArr = [
	{"batchName": "Batch 189"},
	{"batchName": "Batch 190"}
];

console.log(batchesArr);

/*
	stringify method - used to convert JS objects into a string

	Syntax:
		JSON.stringify();
*/
console.log("Result from stringify method:");
console.log(JSON.stringify(batchesArr));

let data = JSON.stringify({
	name: "John",
	age: 31,
	address: {
		city: "City of Manila",
		country: "Philippines"
	}
});

console.log(data);

//User Details
/*let firstName = prompt("What is your first name?");
let lastName = prompt("What is your last name?");
let age = prompt("What is your age?");
let address = {
	city: prompt("Which city do you live in?"),
	country: prompt("Which country does your city address belong to?")
};

let otherData =JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address
});

console.log(otherData);*/

/*
	parse method

	Syntax:
		JSON.parse();
*/

let batchesJSON = `[
	{
		"batchName": "Batch 189"
	},
	{
		"batchName": "Batch 190"
	}
]`;

console.log(batchesJSON);

console.log("Result from parse method:");
console.log(JSON.parse(batchesJSON));

let stringifiedObject = `{
	"name": "Ivy",
	"age": "18",
	"address": {
		"city": "Caloocan City",
		"country": "Philippines"
	}
}`;

console.log(stringifiedObject);
console.log(JSON.parse(stringifiedObject));